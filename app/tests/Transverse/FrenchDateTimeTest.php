<?php

namespace App\Tests\Transverse;

use App\Transverse\FrenchDateTime;
use PHPUnit\Framework\TestCase;

final class FrenchDateTimeTest extends TestCase
{
    public function testFormat(): void
    {
        $date = new FrenchDateTime('2020/10/06');

        static::assertSame(
            'Mardi',
            $date->format('l')
        );

        static::assertSame(
            'Mar',
            $date->format('D')
        );

        static::assertSame(
            'Oct',
            $date->format('M')
        );

        static::assertSame(
            'Octobre',
            $date->format('F')
        );

        static::assertSame(
            'Mardi 6 Octobre 2020',
            $date->format('l j F Y')
        );
    }

    public function testFormat2(): void
    {
        $date = new FrenchDateTime('2020/11/04');

        static::assertSame(
            'Mercredi',
            $date->format('l')
        );

        static::assertSame(
            'Mer',
            $date->format('D')
        );

        static::assertSame(
            'Nov',
            $date->format('M')
        );

        static::assertSame(
            'Novembre',
            $date->format('F')
        );

        static::assertSame(
            'Mercredi 4 Nov 2020',
            $date->format('l j M Y')
        );
    }
}
