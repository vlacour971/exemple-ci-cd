<?php

namespace App\Tests\Weather;

use App\Weather\WeatherDTO;
use App\Weather\WheatherProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class WeatherProviderTest extends TestCase
{

    /**
     * @var WheatherProvider
     */
    private $provider;

    /**
     * @var HttpClientInterface|MockObject
     */
    private $httpClient;

    public function setUp(): void
    {
        $this->httpClient = $this->createMock(HttpClientInterface::class);
        /** @phpstan-ignore-next-line */
        $this->provider = new WheatherProvider($this->httpClient, 'appToken', 'city');
    }

    public function testGetWeather(): void
    {
        $response = $this->createMock(ResponseInterface::class);

        $this->httpClient
            ->method('request')
            ->with('GET', 'http://api.openweathermap.org/data/2.5/weather?q=city&appid=appToken&lang=fr')
            ->willReturn($response);

        $response
            ->method('toArray')
            ->willReturn([
                'weather'   => [
                    [
                        'icon'          => 'type',
                        'description'   => 'description'
                    ]
                ],
                'main'      => [
                    'temp'      => 295.3,
                    'humidity'  => 12.2
                ]
            ]);

        $weather = $this->provider->getWeather();

        self::assertInstanceOf(
            WeatherDTO::class,
            $weather
        );

        self::assertSame(
            'city',
            $weather->getCity()
        );

        self::assertSame(
            'type',
            $weather->getType()
        );

        self::assertSame(
            'description',
            $weather->getDescription()
        );

        self::assertSame(
            22.15,
            $weather->getTemp()
        );

        self::assertSame(
            12.2,
            $weather->getHumidity()
        );
    }
}
