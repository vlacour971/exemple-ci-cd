<?php

namespace App\Tests\Weather;

use App\Weather\WeatherDTO;
use PHPUnit\Framework\TestCase;

final class WeatherDtoTest extends TestCase
{

    /**
     * @var WeatherDTO
     */
    private $dto;

    public function setUp(): void
    {
        $this->dto = new WeatherDTO('city', 'type', 'description', 12, 23);
    }

    public function testGetters(): void
    {
        self::assertSame(
            'city',
            $this->dto->getCity()
        );

        self::assertSame(
            'type',
            $this->dto->getType()
        );

        self::assertSame(
            'description',
            $this->dto->getDescription()
        );

        self::assertSame(
            12,
            $this->dto->getTemp()
        );

        self::assertSame(
            23,
            $this->dto->getHumidity()
        );
    }

}
