<?php

namespace App\Weather;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class WheatherProvider
{
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var string
     */
    private $appToken;

    /**
     * @var string
     */
    private $city;

    /**
     * WheatherProvider constructor.
     */
    public function __construct(HttpClientInterface $httpClient, string $appToken, string $city)
    {
        $this->httpClient = $httpClient;
        $this->appToken = $appToken;
        $this->city = $city;
    }

    public function getWeather(): WeatherDTO
    {
        $response = $this->httpClient->request(
            'GET',
            sprintf('http://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&lang=fr', $this->city, $this->appToken)
        );

        $results = $response->toArray();

        return new WeatherDTO(
            $this->city,
            $results['weather'][0]['icon'],
            $results['weather'][0]['description'],
            ($results['main']['temp'] - 273.15),
            $results['main']['humidity']
        );
    }
}
