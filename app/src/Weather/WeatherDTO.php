<?php

namespace App\Weather;

class WeatherDTO
{
    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $temp;

    /**
     * @var float
     */
    private $humidity;

    /**
     * WeatherDTO constructor.
     *
     * @param string $city
     * @param string $type
     * @param string $description
     * @param float  $temp
     * @param float  $humidity
     */
    public function __construct($city, $type, $description, $temp, $humidity)
    {
        $this->city = $city;
        $this->type = $type;
        $this->description = $description;
        $this->temp = $temp;
        $this->humidity = $humidity;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return float
     */
    public function getTemp()
    {
        return $this->temp;
    }

    /**
     * @return float
     */
    public function getHumidity()
    {
        return $this->humidity;
    }
}
