<?php

namespace App\Transverse;

class FrenchDateTime extends \DateTime
{
    const DAYS_FULL = [
        'Monday'    => 'Lundi',
        'Tuesday'   => 'Mardi',
        'Wednesday' => 'Mercredi',
        'Thursday'  => 'Jeudi',
        'Friday'    => 'Vendredi',
        'Saturday'  => 'Samedi',
        'Sunday'    => 'Dimanche',
    ];

    const DAYS_SMALL = [
        'Mon' => 'Lun',
        'Tue' => 'Mar',
        'Wed' => 'Mer',
        'Thu' => 'Jeu',
        'Fri' => 'Ven',
        'Sat' => 'Sam',
        'Sun' => 'Dim',
    ];

    const MONTHS_FULL = [
        'January'   => 'Janvier',
        'February'  => 'Février',
        'March'     => 'Mars',
        'April'     => 'Avril',
        'May'       => 'Mai',
        'June'      => 'Juin',
        'July'      => 'Juillet',
        'August'    => 'Août',
        'September' => 'Septembre',
        'October'   => 'Octobre',
        'November'  => 'Novembre',
        'December'  => 'Décembre',
    ];

    const MONTHS_SMALL = [
        'Feb' => 'Fév',
        'Apr' => 'Avr',
        'May' => 'Mai',
        'Jun' => 'Juin',
        'Jul' => 'Juil',
        'Aug' => 'Août',
        'Dec' => 'Déc',
    ];

    public function format($format = 'j M Y')
    {
        $display = parent::format($format);

        if (strstr($format, 'l')) {
            $display = str_replace(array_keys(self::DAYS_FULL), array_values(self::DAYS_FULL), $display);
        }
        if (strstr($format, 'D')) {
            $display = str_replace(array_keys(self::DAYS_SMALL), array_values(self::DAYS_SMALL), $display);
        }
        if (strstr($format, 'F')) {
            $display = str_replace(array_keys(self::MONTHS_FULL), array_values(self::MONTHS_FULL), $display);
        }
        if (strstr($format, 'M')) {
            $display = str_replace(array_keys(self::MONTHS_SMALL), array_values(self::MONTHS_SMALL), $display);
        }

        return $display;
    }
}
