<?php

namespace App\Controller;

use App\Transverse\FrenchDateTime;
use App\Weather\WeatherDTO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController
{
    public function __invoke(WeatherDTO $weatherDTO): Response
    {
        return $this->render(
            'index.html.twig',
            [
                'weather' => $weatherDTO,
                'date'    => new FrenchDateTime(),
            ]
        );
    }
}
